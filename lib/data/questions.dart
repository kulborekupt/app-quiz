import 'package:app_quiz/models/quiz_questions.dart';

const questions = [
  QuizQuestions('Hello World ', [
    'ans1eee',
    'ans2wwddd',
    'ans3',
    'ans4',
  ]),
  QuizQuestions('Hello World  2', [
    'ans2111',
    'ans2ddddd',
  ]),
  QuizQuestions('Hello World 3', [
    'ans2333',
    'ans1ddddddd',
  ]),
  QuizQuestions('Hello World 3', [
    'ans2333',
    'ans1ddddddd',
  ]),
  QuizQuestions('Hello World 3', [
    'ans2333',
    'ans1ddddddd',
  ])
];
