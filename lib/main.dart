import 'package:app_quiz/quiz.dart';
import 'package:flutter/material.dart';

void main() {
  const quiz = Quiz();
  runApp(quiz);
}
